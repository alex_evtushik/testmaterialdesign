package test.materialdesign.ui.fragments;

import android.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.getbase.floatingactionbutton.FloatingActionButton;
import com.getbase.floatingactionbutton.FloatingActionsMenu;

import test.materialdesign.R;

/**
 * Created by Alexandr on 20.02.2015.
 */
public class FloatingActionsMenuFragment extends Fragment {

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.floation_actions_menu, container, false);

        FloatingActionsMenu upMenu = (FloatingActionsMenu) rootView.findViewById(R.id.multiple_actions_up);
        FloatingActionButton actionC = (FloatingActionButton) rootView.findViewById(R.id.actionC);
        actionC.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(getActivity().getApplicationContext(), "Action C", Toast.LENGTH_SHORT).show();
            }
        });
        FloatingActionButton actionB = (FloatingActionButton) rootView.findViewById(R.id.actionB);
        actionB.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(getActivity().getApplicationContext(), "Action B", Toast.LENGTH_SHORT).show();
            }
        });
        FloatingActionButton actionA = (FloatingActionButton) rootView.findViewById(R.id.actionA);
        actionA.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(getActivity().getApplicationContext(), "Action A", Toast.LENGTH_SHORT).show();
            }
        });


        FloatingActionsMenu leftMenu = (FloatingActionsMenu) rootView.findViewById(R.id.multiple_actions_left);


        return rootView;
    }
}

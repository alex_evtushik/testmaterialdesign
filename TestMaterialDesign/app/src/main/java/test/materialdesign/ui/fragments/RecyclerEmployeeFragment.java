package test.materialdesign.ui.fragments;

import android.app.Fragment;
import android.app.LoaderManager;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v4.widget.SwipeRefreshLayout.OnRefreshListener;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import test.materialdesign.R;
import test.materialdesign.adapters.DataAdapter;
import test.materialdesign.databases.employee.EmployeeCursorLoader;
import test.materialdesign.databases.employee.EmployeeMainCursorLoader;
import test.materialdesign.listeners.OnViewHolderListener;
import test.materialdesign.ui.preferences.SharedPreferencesHelper;
import test.materialdesign.utils.Utils;

/**
 * Created by Alexandr on 13.02.2015.
 */
public class RecyclerEmployeeFragment extends Fragment {
    public static final String TAG = RecyclerEmployeeFragment.class.getSimpleName();
    private final int LOADER_ID = 1;
    private RecyclerView mRecyclerView;
    private LinearLayoutManager mLayoutManager;
    private DataAdapter mDataAdapter;
    private EmployeeCursorLoader mCursorLoader;
    private EmployeeMainCursorLoader mMainCursorLoader;
    private SwipeRefreshLayout mSwipeRefresh;
    private LoaderManager mLoaderManager;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_recycler_swipe_refresh, container, false);

        mRecyclerView = (RecyclerView) rootView.findViewById(R.id.recycler_view);
        mSwipeRefresh = (SwipeRefreshLayout) rootView.findViewById(R.id.swipe_refresh);
        mLayoutManager = new LinearLayoutManager(getActivity());
        mDataAdapter = new DataAdapter(null);

        mSwipeRefresh.setOnRefreshListener(refreshListener);
        mSwipeRefresh.setColorSchemeResources(android.R.color.holo_blue_light, android.R.color.holo_orange_light, android.R.color.holo_red_light);

        mRecyclerView.setHasFixedSize(true);
        mRecyclerView.setLayoutManager(mLayoutManager);
        mRecyclerView.setAdapter(mDataAdapter);

        mDataAdapter.setOnViewHolderListener(viewHolderListener);

        mCursorLoader = new EmployeeCursorLoader(getActivity(), mDataAdapter, mDataAdapter.LIMIT);
        mLoaderManager = getLoaderManager();
        mLoaderManager.initLoader(LOADER_ID, null, mCursorLoader);

        return rootView;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        boolean isFill = SharedPreferencesHelper.isFill(getActivity());
        if (!isFill) {
            Utils.fillDB(getActivity(), 100);
            SharedPreferencesHelper.fillDB(getActivity(), true);
        }
    }

    private OnViewHolderListener viewHolderListener = new OnViewHolderListener() {
        @Override
        public void onRequestedLastItem(int requestCount) {
            mCursorLoader = new EmployeeCursorLoader(getActivity(), mDataAdapter, requestCount);

            new Thread() {
                @Override
                public void run() {
                    try {
                        sleep(2000);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                    RecyclerEmployeeFragment.this.getActivity().runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            mLoaderManager.restartLoader(LOADER_ID, null, mCursorLoader);
                        }
                    });
                }
            }.start();
        }

        @Override
        public void onRequestedFirstItem() {}
    };

    private OnRefreshListener refreshListener = new OnRefreshListener() {
        @Override
        public void onRefresh() {
            Log.d(TAG, "Refresh !");
            stopRefreshing();
        }
    };

    private void stopRefreshing() {
        if (mSwipeRefresh != null)
            mSwipeRefresh.setRefreshing(false);
    }
}

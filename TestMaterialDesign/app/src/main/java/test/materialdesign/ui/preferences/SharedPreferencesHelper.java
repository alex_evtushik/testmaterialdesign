package test.materialdesign.ui.preferences;

import android.content.Context;
import android.content.SharedPreferences;

/**
 * Created by Alexandr on 18.02.2015.
 */
public class SharedPreferencesHelper {
    private static final String PREF_FILENAME = "pref";

    private SharedPreferencesHelper() {}

    public static void fillDB(Context context, boolean fill) {
        SharedPreferences preferences = context.getSharedPreferences(PREF_FILENAME, Context.MODE_PRIVATE);
        SharedPreferences.Editor edit = preferences.edit();
        edit.putBoolean(Keys.FILL_DB, fill);
        edit.commit();
    }

    public static boolean isFill(Context context) {
        SharedPreferences preferences = context.getSharedPreferences(PREF_FILENAME, Context.MODE_PRIVATE);
        return preferences.getBoolean(Keys.FILL_DB, false);
    }


    private static class Keys {
        private static final String FILL_DB = "fill_db";
    }
}

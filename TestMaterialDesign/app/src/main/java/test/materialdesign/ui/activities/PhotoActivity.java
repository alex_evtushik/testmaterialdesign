package test.materialdesign.ui.activities;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.widget.ImageView;

import test.materialdesign.R;

/**
 * Created by Alexandr on 18.02.2015.
 */
public class PhotoActivity extends Activity {
    public static final String TAG = PhotoActivity.class.getSimpleName();
    private ImageView mImageView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.photo_activity);

        mImageView = (ImageView) findViewById(R.id.image_view);

        Intent intent = getIntent();
        if (intent != null) {
            mImageView.setImageResource(intent.getIntExtra("img_res", R.drawable.ic_launcher));
        }
    }
}

package test.materialdesign.ui.fragments;

import android.app.Fragment;
import android.content.Context;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import com.h6ah4i.android.widget.advrecyclerview.swipeable.RecyclerViewSwipeManager;
import com.h6ah4i.android.widget.advrecyclerview.touchguard.RecyclerViewTouchActionGuardManager;

import test.materialdesign.R;
import test.materialdesign.adapters.RecyclerCardAdapter;
import test.materialdesign.adapters.SwipeableRecyclerAdapter;

/**
 * Created by Alexandr on 18.02.2015.
 */
public class RecyclerCardFragment extends Fragment {
    public static final String TAG = RecyclerCardFragment.class.getSimpleName();
    private Context mContext;
    private RecyclerView mRecyclerView;
    private RecyclerCardAdapter mCardAdapter;
    private LinearLayoutManager mLayoutManager;

    private SwipeableRecyclerAdapter mAdapter;
    private RecyclerView.Adapter mWrappedAdapter;
    private RecyclerViewSwipeManager mRecyclerViewSwipeManager;
    private RecyclerViewTouchActionGuardManager mRecyclerViewTouchActionGuardManager;

    public RecyclerCardFragment(Context context) {
        mContext = context;
    }

    public static RecyclerCardFragment newInstance(Context context) {
        RecyclerCardFragment fragment = new RecyclerCardFragment(context);
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_recycler, container, false);
        mRecyclerView = (RecyclerView) rootView.findViewById(R.id.recycler_view);

        mLayoutManager = new LinearLayoutManager(getActivity());
        mCardAdapter = new RecyclerCardAdapter();

        mRecyclerView.setAdapter(mCardAdapter);
        mRecyclerView.setHasFixedSize(true);
//        mRecyclerView.setItemAnimator(new SwipeDismissItemAnimator());
        mRecyclerView.setLayoutManager(mLayoutManager);


        return rootView;
    }



    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        setHasOptionsMenu(true);

    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.fragment_menu, menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int position = mLayoutManager.findFirstCompletelyVisibleItemPosition();

        switch (item.getItemId()) {
            case R.id.add_item:
                mCardAdapter.addItem(position);
                return true;
            case R.id.remove_item:
                mCardAdapter.removeItem(position);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}

package test.materialdesign.ui.fragments;

import android.app.Fragment;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.v7.graphics.Palette;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.io.IOException;

import test.materialdesign.R;

import static android.provider.MediaStore.Images;

/**
 * Created by Alexandr on 20.02.2015.
 */
public class PaletteFragment extends Fragment {
    public static final String TAG = PaletteFragment.class.getSimpleName();
    private static final int RESULT_LOAD_IMAGE = 1;
    private final int DEFAULT_COLOR = android.R.color.transparent;
    private ImageView mImageView;
    private TextView mTextViewVibrant, mTextViewLightVibrant, mTextViewDarkVibrant,
            mTextViewMuted, mTextViewLightMuted, mTextViewDarkMuted;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        final View rootView = inflater.inflate(R.layout.palette_fragment, container, false);
        mImageView = (ImageView) rootView.findViewById(R.id.image_view);
        mTextViewVibrant = (TextView) rootView.findViewById(R.id.vibrant);
        mTextViewLightVibrant = (TextView) rootView.findViewById(R.id.vibrantLight);
        mTextViewDarkVibrant = (TextView) rootView.findViewById(R.id.vibrantDark);
        mTextViewMuted = (TextView) rootView.findViewById(R.id.muted);
        mTextViewLightMuted = (TextView) rootView.findViewById(R.id.mutedLight);
        mTextViewDarkMuted = (TextView) rootView.findViewById(R.id.mutedDark);

        generatePaletteFromBitmap(BitmapFactory.decodeResource(getActivity().getResources(), R.drawable.img));


        (rootView.findViewById(R.id.choose_image)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent galleryIntent = new Intent(Intent.ACTION_PICK, Images.Media.EXTERNAL_CONTENT_URI);
                startActivityForResult(galleryIntent, RESULT_LOAD_IMAGE);
            }
        });

        return rootView;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == RESULT_LOAD_IMAGE && data != null) {
            Uri selectedImage = data.getData();

            try {
                generatePaletteFromBitmap(Images.Media.getBitmap(getActivity().getContentResolver(), selectedImage));

            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    private void generatePaletteFromBitmap(Bitmap bitmap) {
        mImageView.setImageBitmap(bitmap);
        Palette.generateAsync(bitmap, new Palette.PaletteAsyncListener() {

            @Override
            public void onGenerated(Palette palette) {
                getColorFromPalette(palette);
                getColorFromPaletteSwatch(palette);
            }
        });
    }

    private void getColorFromPalette(Palette palette) {
        int vibrant = palette.getVibrantColor(DEFAULT_COLOR);
        int vibrantLight = palette.getLightVibrantColor(DEFAULT_COLOR);
        int vibrantDark = palette.getDarkVibrantColor(DEFAULT_COLOR);
        int muted = palette.getMutedColor(DEFAULT_COLOR);
        int mutedLight = palette.getLightMutedColor(DEFAULT_COLOR);
        int mutedDark = palette.getDarkMutedColor(DEFAULT_COLOR);

        mTextViewVibrant.setBackgroundColor(vibrant);
        mTextViewLightVibrant.setBackgroundColor(vibrantLight);
        mTextViewDarkVibrant.setBackgroundColor(vibrantDark);
        mTextViewMuted.setBackgroundColor(muted);
        mTextViewLightMuted.setBackgroundColor(mutedLight);
        mTextViewDarkMuted.setBackgroundColor(mutedDark);
    }

    private void getColorFromPaletteSwatch(Palette palette) {
        Palette.Swatch vibrantSwatch = palette.getVibrantSwatch();
        Palette.Swatch vibrantLightSwatch = palette.getLightVibrantSwatch();
        Palette.Swatch vibrantDarkSwatch = palette.getDarkVibrantSwatch();
        Palette.Swatch mutedSwatch = palette.getMutedSwatch();
        Palette.Swatch mutedLightSwatch = palette.getLightMutedSwatch();
        Palette.Swatch mutedDarkSwatch = palette.getDarkMutedSwatch();

        if (vibrantSwatch != null) {
            mTextViewVibrant.setTextColor(vibrantSwatch.getTitleTextColor());
//            mTextViewVibrant.setBackgroundColor(vibrantSwatch.getBodyTextColor());
        }
        if (vibrantLightSwatch != null) {
            mTextViewLightVibrant.setTextColor(vibrantLightSwatch.getTitleTextColor());
//            mTextViewLightVibrant.setBackgroundColor(vibrantLightSwatch.getBodyTextColor());
        }
        if (vibrantDarkSwatch != null) {
            mTextViewDarkVibrant.setTextColor(vibrantDarkSwatch.getTitleTextColor());
//            mTextViewDarkVibrant.setBackgroundColor(vibrantDarkSwatch.getBodyTextColor());
        }
        if (mutedSwatch != null) {
            mTextViewMuted.setTextColor(mutedSwatch.getTitleTextColor());
//            mTextViewMuted.setBackgroundColor(mutedSwatch.getBodyTextColor());
        }
        if (mutedLightSwatch != null) {
            mTextViewLightMuted.setTextColor(mutedLightSwatch.getTitleTextColor());
//            mTextViewLightMuted.setBackgroundColor(mutedLightSwatch.getBodyTextColor());
        }
        if (mutedDarkSwatch != null) {
            mTextViewDarkMuted.setTextColor(mutedDarkSwatch.getTitleTextColor());
//            mTextViewDarkMuted.setBackgroundColor(mutedDarkSwatch.getBodyTextColor());
        }
    }
}

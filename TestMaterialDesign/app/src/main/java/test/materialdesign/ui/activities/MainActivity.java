package test.materialdesign.ui.activities;

import android.app.Fragment;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.Menu;
import android.view.MenuItem;

import test.materialdesign.R;
import test.materialdesign.ui.fragments.DialogFragment;
import test.materialdesign.ui.fragments.FloatingActionsMenuFragment;
import test.materialdesign.ui.fragments.NotificationFragment;
import test.materialdesign.ui.fragments.PaletteFragment;
import test.materialdesign.ui.fragments.RecyclerCardFragment;
import test.materialdesign.ui.fragments.RecyclerEmployeeFragment;
import test.materialdesign.ui.fragments.RecyclerGridFragment;
import test.materialdesign.ui.fragments.RevealEffectFragment;
import test.materialdesign.ui.fragments.ShadowClippingFragment;
import test.materialdesign.ui.fragments.ViewStateFragment;


public class MainActivity extends ActionBarActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.card:
                showFragment(RecyclerCardFragment.newInstance(this));
                return true;
            case R.id.grid:
                showFragment(new RecyclerGridFragment());
                return true;
            case R.id.employee:
                showFragment(new RecyclerEmployeeFragment());
                return true;
            case R.id.reveal:
                showFragment(new RevealEffectFragment());
                return true;
            case R.id.state:
                showFragment(new ViewStateFragment());
            case R.id.shadow:
                showFragment(new ShadowClippingFragment());
                return true;
            case R.id.fab:
                showFragment(new FloatingActionsMenuFragment());
                return true;
            case R.id.dialog:
                showFragment(DialogFragment.newInstance());
                return true;
            case R.id.notification:
                showFragment(NotificationFragment.newInstance());
                return true;
            case R.id.palette:
                showFragment(new PaletteFragment());
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void showFragment(Fragment fragment) {
        getFragmentManager().beginTransaction()
                .setCustomAnimations(R.animator.gla_there_come, R.animator.gla_there_gone)
                .replace(R.id.container, fragment).commit();
    }
}

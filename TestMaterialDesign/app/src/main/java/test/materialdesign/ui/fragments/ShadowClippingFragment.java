package test.materialdesign.ui.fragments;

import android.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import test.materialdesign.R;

/**
 * Created by Alexandr on 19.02.2015.
 */
public class ShadowClippingFragment extends Fragment {

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.shadow_clipping, container, false);
        return rootView;
    }
}

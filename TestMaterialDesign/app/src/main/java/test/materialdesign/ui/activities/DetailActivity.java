package test.materialdesign.ui.activities;

import android.app.Activity;
import android.os.Bundle;

import test.materialdesign.R;

/**
 * Created by Alexandr on 17.02.2015.
 */
public class DetailActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.detail_activity);
    }
}

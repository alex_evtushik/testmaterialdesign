package test.materialdesign.ui.fragments;

import android.app.Fragment;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import test.materialdesign.R;
import test.materialdesign.adapters.RecyclerAdapter;

/**
 * Created by Alexandr on 12.02.2015.
 */
public class RecyclerGridFragment extends Fragment {
    public static final String TAG = RecyclerGridFragment.class.getSimpleName();
    private static final int SPAN_COUNT = 4;
    private RecyclerView mRecyclerView;
    private RecyclerAdapter mGridAdapter;
    private LinearLayoutManager mLayoutManager;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_recycler, container, false);

        mRecyclerView = (RecyclerView) rootView.findViewById(R.id.recycler_view);

        mLayoutManager = new GridLayoutManager(getActivity(), SPAN_COUNT);
        mGridAdapter = new RecyclerAdapter();
        mRecyclerView.setAdapter(mGridAdapter);

        mRecyclerView.setHasFixedSize(true);
        mRecyclerView.setItemAnimator(new DefaultItemAnimator());
        mRecyclerView.setLayoutManager(mLayoutManager);

        return rootView;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.fragment_menu, menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int position = mLayoutManager.findFirstCompletelyVisibleItemPosition();

        switch (item.getItemId()) {
            case R.id.add_item:
                mGridAdapter.addItem(position);
                return true;
            case R.id.remove_item:
                mGridAdapter.removeItem(position);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}

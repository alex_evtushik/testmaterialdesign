package test.materialdesign.ui.fragments;

import android.animation.Animator;
import android.annotation.TargetApi;
import android.app.Fragment;
import android.os.Build;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewAnimationUtils;
import android.view.ViewGroup;
import android.view.animation.AccelerateDecelerateInterpolator;
import android.widget.Button;

import test.materialdesign.R;

/**
 * Created by Alexandr on 19.02.2015.
 */
public class RevealEffectFragment extends Fragment {
    private final long DURATION = 500L;
    private View mView;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        final View rootView = inflater.inflate(R.layout.reveal_effect, container, false);
        mView = rootView.findViewById(R.id.circle);
        mView.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                int cx = (int) event.getX();
                int cy = (int) event.getY();
                float finalRadius = Math.max(mView.getWidth(), mView.getHeight());
                startReveal(cx, cy, 0, finalRadius);
                return false;
            }
        });


        Button center = (Button) rootView.findViewById(R.id.center);
        center.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {
                int cx = (mView.getLeft() + mView.getRight()) / 2;
                int cy = (mView.getTop() + mView.getBottom()) / 2;
                float finalRadius = Math.max(mView.getWidth(), mView.getHeight());

                startReveal(cx, cy, 0, finalRadius);
            }
        });

        Button right = (Button) rootView.findViewById(R.id.right);
        right.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {
                int cx = mView.getRight();
                int cy = (mView.getTop() + mView.getBottom()) / 2;
                float finalRadius = Math.max(mView.getWidth(), mView.getHeight());

                startReveal(cx, cy, 0, finalRadius);
            }
        });

        Button left = (Button) rootView.findViewById(R.id.left);
        left.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {
                int cx = mView.getLeft();
                int cy = (mView.getTop() + mView.getBottom()) / 2;
                float finalRadius = Math.max(mView.getWidth(), mView.getHeight());

                startReveal(cx, cy, 0, finalRadius);
            }
        });

        return rootView;
    }

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    private void startReveal(int cx, int cy, float startRadius, float finalRadius) {

        // Create a reveal {@link Animator} that starts clipping the view from
        // the top left corner until the whole view is covered.
        Animator animator = ViewAnimationUtils.createCircularReveal(mView, cx, cy, startRadius, finalRadius);

        // Set a natural ease-in/ease-out interpolator.
        animator.setInterpolator(new AccelerateDecelerateInterpolator());
        // Set time duration
        animator.setDuration(DURATION);
        animator.start();
    }
}

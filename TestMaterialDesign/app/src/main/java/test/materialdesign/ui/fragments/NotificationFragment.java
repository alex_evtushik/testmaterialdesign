package test.materialdesign.ui.fragments;

import android.annotation.TargetApi;
import android.app.Fragment;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.BitmapFactory;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.NotificationCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.RemoteViews;


import test.materialdesign.R;
import test.materialdesign.ui.activities.MainActivity;

/**
 * Created by Alexandr on 19.03.2015.
 */
public class NotificationFragment extends Fragment {
    private static final String TAG = NotificationFragment.class.getSimpleName();
    private static final int NOTIFY_ID = 101;
    private static final int PRIORITY = 100;
    private Button showNotification;
    private Context mContext;
    private NotificationManager mNotificationManager;
    private Intent mNotificationIntent;
    private PendingIntent mContentIntent;

    public NotificationFragment() {}

    public static NotificationFragment newInstance() {
        NotificationFragment fragment = new NotificationFragment();
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.notification_fragment, container, false);

        mContext = getActivity();
        mNotificationManager = (NotificationManager) mContext.getSystemService(Context.NOTIFICATION_SERVICE);
        mNotificationIntent = new Intent(mContext, MainActivity.class);
        mContentIntent = PendingIntent.getActivity(mContext, 0, mNotificationIntent, PendingIntent.FLAG_CANCEL_CURRENT);

        showNotification = (Button) rootView.findViewById(R.id.show_notification);
        showNotification.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                    showLollipopNotification();
                    return;
                }
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
                    showJellyBeanNotification();
                    return;
                }
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.ICE_CREAM_SANDWICH) {
                    showIceCreameNotification();
                    return;
                }
            }
        });

        return rootView;
    }

    @TargetApi(Build.VERSION_CODES.JELLY_BEAN)
    private void showJellyBeanNotification() {
        NotificationCompat.BigTextStyle notificationStyle = new NotificationCompat.BigTextStyle();
        notificationStyle.bigText("The notifications framework allows you" +
                "\nto define a custom notification layout," +
                "\nwhich defines the notification's " +
                "\nappearance in a RemoteViews object.");

        Notification notification = new NotificationCompat.Builder(mContext)
                .setContentIntent(mContentIntent)
                .setSmallIcon(R.drawable.ic_access_time)
                .setLargeIcon(BitmapFactory.decodeResource(mContext.getResources(), R.drawable.ic_access_time_grey))
                .addAction(R.drawable.ic_close, "Cancel", mContentIntent)
                .addAction(R.drawable.ic_check, "Ok", mContentIntent)
                .setContentTitle("Big Content View")
                .setWhen(System.currentTimeMillis())
                .setPriority(PRIORITY)
                .setDefaults(Notification.DEFAULT_SOUND | Notification.DEFAULT_VIBRATE)
                .setAutoCancel(true)
                .setStyle(notificationStyle)
                .build();

        mNotificationManager.notify(NOTIFY_ID, notification);

    }

    private void showIceCreameNotification() {
        Notification notification = new Notification.Builder(mContext)
                .setContentIntent(mContentIntent)
                .setSmallIcon(R.drawable.ic_access_time)
                .setLargeIcon(BitmapFactory.decodeResource(mContext.getResources(), R.drawable.ic_access_time_grey))
                .setContentTitle("Напоминание")
                .setContentText("Пора покормить котиков")
                .setWhen(System.currentTimeMillis())
                .setDefaults(Notification.DEFAULT_SOUND | Notification.DEFAULT_VIBRATE)
                .setAutoCancel(true)
                .getNotification();

        mNotificationManager.notify(NOTIFY_ID, notification);
    }

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    private void showLollipopNotification() {
        Notification notification = new Notification.Builder(mContext)
                .setVisibility(Notification.VISIBILITY_PUBLIC)
                .setContentIntent(mContentIntent)
                .setSmallIcon(R.drawable.ic_access_time)
                .setLargeIcon(BitmapFactory.decodeResource(mContext.getResources(), R.drawable.ic_access_time_grey))
                .setContentTitle("Напоминание")
                .setContentText("Пора покормить котиков")
                .addAction(R.drawable.ic_close, "Cancel", mContentIntent)
                .addAction(R.drawable.ic_check, "Ok", mContentIntent)
                .setWhen(System.currentTimeMillis())
                .setPriority(PRIORITY)
                .setDefaults(Notification.DEFAULT_SOUND | Notification.DEFAULT_VIBRATE)
                .setAutoCancel(true)
                .build();

        mNotificationManager.notify(NOTIFY_ID, notification);
    }

    @TargetApi(Build.VERSION_CODES.JELLY_BEAN)
    private void showCustomNotification() {
        RemoteViews views = new RemoteViews(mContext.getPackageName(), R.layout.notification_custom_layout);

        views.setContentDescription(R.id.notification_title, "Custom title");
        views.setContentDescription(R.id.notification_content, "The notifications framework allows you" +
                "\nto define a custom notification layout," +
                "\nwhich defines the notification's " +
                "\nappearance in a RemoteViews object.");

        views.setOnClickPendingIntent(R.id.notification_btn_ok, mContentIntent);
        views.setOnClickPendingIntent(R.id.notification_btn_cancel, mContentIntent);

        Notification notification = new NotificationCompat.Builder(mContext)
                .setContentIntent(mContentIntent)
                .setSmallIcon(R.drawable.ic_access_time)
                .setContent(views)
                .setDefaults(Notification.DEFAULT_SOUND | Notification.DEFAULT_VIBRATE)
                .setAutoCancel(true)
                .build();

        mNotificationManager.notify(NOTIFY_ID, notification);
    }
}

package test.materialdesign.ui.fragments;

import android.annotation.TargetApi;
import android.app.AlertDialog;
import android.app.Fragment;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Toast;

import com.afollestad.materialdialogs.MaterialDialog;

import test.materialdesign.R;

/**
 * Created by Alexandr on 19.03.2015.
 */
public class DialogFragment extends Fragment {
    public static final String TAG = DialogFragment.class.getSimpleName();
    private Button basicDialog, singleChoiceListDialog, multiChoiceListDialog, customViewDialog, progressDialog,
    basicMaterialDialog, singleChoiceListMaterialDialog, multiChoiceListMaterialDialog, customViewMaterialDialog, progressDialogMaterial;

    private String[] fontSizeArray = { "Small", "Normal", "Large", "Huge" };
    private int singleChoice = 1;
    private String[] socialNetworks = { "google plus", "facebook", "instagram", "twitter", "linked Id", "vk" };
    private boolean[] multiChoice = { false, false, false, false, true, true };


    public static DialogFragment newInstance() {
        DialogFragment fragment = new DialogFragment();
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.dialog_fragment, container, false);

        basicDialog = (Button) rootView.findViewById(R.id.basic);
        singleChoiceListDialog = (Button) rootView.findViewById(R.id.single_choice);
        multiChoiceListDialog = (Button) rootView.findViewById(R.id.multi_choice);
        customViewDialog = (Button) rootView.findViewById(R.id.custom);
        progressDialog = (Button) rootView.findViewById(R.id.progress_dialog);

        basicDialog.setOnClickListener(dialogListener);
        singleChoiceListDialog.setOnClickListener(dialogListener);
        multiChoiceListDialog.setOnClickListener(dialogListener);
        customViewDialog.setOnClickListener(dialogListener);
        progressDialog.setOnClickListener(dialogListener);


        basicMaterialDialog = (Button) rootView.findViewById(R.id.basic_material);
        singleChoiceListMaterialDialog = (Button) rootView.findViewById(R.id.single_choice_material);
        multiChoiceListMaterialDialog = (Button) rootView.findViewById(R.id.multi_choice_material);
        customViewMaterialDialog = (Button) rootView.findViewById(R.id.custom_material);
        progressDialogMaterial = (Button) rootView.findViewById(R.id.progress_dialog_material);

        basicMaterialDialog.setOnClickListener(materialDialogListener);
        singleChoiceListMaterialDialog.setOnClickListener(materialDialogListener);
        multiChoiceListMaterialDialog.setOnClickListener(materialDialogListener);
        customViewMaterialDialog.setOnClickListener(materialDialogListener);
        progressDialogMaterial.setOnClickListener(materialDialogListener);

        return rootView;
    }

    private View.OnClickListener dialogListener = new View.OnClickListener() {

        @Override
        public void onClick(View v) {
            switch (v.getId()) {
                case R.id.basic:
                    showBasicDialog();
                    break;
                case R.id.single_choice:
                    showSingleChoiceListDialog();
                    break;
                case R.id.multi_choice:
                    showMultiChoiceDialog();
                    break;
                case R.id.custom:
                    showCustomViewDialog();
                    break;
                case R.id.progress_dialog:
                    showProgressDialog();
                    break;
            }
        }
    };

    private View.OnClickListener materialDialogListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            switch (v.getId()) {
                case R.id.basic_material:
                    showMaterialBasicDialog();
                    break;
                case R.id.single_choice_material:
                    showMaterialSingleChoiceListDialog();
                    break;
                case R.id.multi_choice_material:
                    showMaterialMultiChoiceDialog();
                    break;
                case R.id.custom_material:
                    showMaterialCustomViewDialog();
                    break;
                case R.id.progress_dialog_material:
                    showMaterialProgressDialog();
                    break;
            }
        }
    };



    private void showBasicDialog() {
        new AlertDialog.Builder(getActivity())
                .setTitle("Использовать сервис Google Location?")
                .setMessage("Это позволит Google приложениям определать ваше местоположения.")
                .setPositiveButton("Согласен", null)
                .setNegativeButton("Не согласен", null)
                .show();
    }

    private void showSingleChoiceListDialog() {
        new AlertDialog.Builder(getActivity())
                .setTitle("Font size")
                .setSingleChoiceItems(fontSizeArray, singleChoice, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        singleChoice = which;
                        dialog.dismiss();
                        Toast.makeText(getActivity(), "Choose " + fontSizeArray[which], Toast.LENGTH_LONG).show();
                    }
                })
                .show();
    }

    private void showMultiChoiceDialog() {
        new AlertDialog.Builder(getActivity())
                .setTitle("Social Networks")
                .setMultiChoiceItems(socialNetworks, multiChoice, null)
                .setPositiveButton("Choose", null)
                .show();
    }

    private void showCustomViewDialog() {
        LayoutInflater inflater = (LayoutInflater) getActivity().getSystemService(getActivity().LAYOUT_INFLATER_SERVICE);
        View view = inflater.inflate(R.layout.dialog_custom_view, null);

        new AlertDialog.Builder(getActivity())
                .setTitle("Google Wi-Fi")
                .setPositiveButton("Connect", null)
                .setNegativeButton("Cancel", null)
                .setView(view)
                .show();
    }

    private void showProgressDialog() {
        ProgressDialog dialog = new ProgressDialog(getActivity());
        dialog.setTitle("Progress Dialog");
        dialog.setMessage("Please wait...");
        dialog.setIndeterminate(true);
        dialog.show();
    }

    private void showMaterialBasicDialog() {
        new MaterialDialog.Builder(getActivity())
                .title("Использовать сервис Google Location?")
                .content("Это позволит Google приложениям определать ваше местоположения.")
                .positiveText("Agree")
                .negativeText("Disagree")
                .neutralText("More info")
                .show();
    }

    private void showMaterialSingleChoiceListDialog() {
        new MaterialDialog.Builder(getActivity())
                .title("Font size")
                .items(fontSizeArray)
                .itemsCallbackSingleChoice(singleChoice, new MaterialDialog.ListCallback() {
                    @Override
                    public void onSelection(MaterialDialog materialDialog, View view, int i, CharSequence charSequence) {
                        singleChoice = i;
                        materialDialog.dismiss();
                        Toast.makeText(getActivity(), "Choose " + fontSizeArray[i], Toast.LENGTH_LONG).show();

                    }
                })
                .show();
    }

    private void showMaterialMultiChoiceDialog() {
        new MaterialDialog.Builder(getActivity())
                .title("Social Networks")
                .items(socialNetworks)
                .itemsCallbackMultiChoice(null, new MaterialDialog.ListCallbackMulti() {
                    @Override
                    public void onSelection(MaterialDialog materialDialog, Integer[] integers, CharSequence[] charSequences) {

                    }
                })
                .positiveText("Choose")
                .show();
    }

    private void showMaterialCustomViewDialog() {
        LayoutInflater inflater = (LayoutInflater) getActivity().getSystemService(getActivity().LAYOUT_INFLATER_SERVICE);
        View view = inflater.inflate(R.layout.dialog_custom_view, null);

        new MaterialDialog.Builder(getActivity())
                .title("Google Wi-Fi")
                .positiveText("Connect")
                .negativeText("Cancel")
                .customView(view, true)
                .show();

    }

    private void showMaterialProgressDialog() {
        new MaterialDialog.Builder(getActivity())
                .title("Progress dialog")
                .progress(true, 0)
                .content("Please wait..")
                .show();
    }

}

package test.materialdesign.listeners;

/**
 * Created by Alexandr on 20.02.2015.
 */
public interface OnViewHolderListener {

    public void onRequestedLastItem(int requestCount);
    public void onRequestedFirstItem();
}

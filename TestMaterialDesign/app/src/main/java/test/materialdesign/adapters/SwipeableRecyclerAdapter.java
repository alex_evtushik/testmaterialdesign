package test.materialdesign.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.h6ah4i.android.widget.advrecyclerview.swipeable.RecyclerViewSwipeManager;
import com.h6ah4i.android.widget.advrecyclerview.swipeable.SwipeableItemAdapter;
import com.h6ah4i.android.widget.advrecyclerview.utils.AbstractSwipeableItemViewHolder;

import test.materialdesign.R;

/**
 * Created by Alexandr on 17.03.2015.
 */
public class SwipeableRecyclerAdapter extends RecyclerView.Adapter<SwipeableRecyclerAdapter.ViewHolder> implements SwipeableItemAdapter<SwipeableRecyclerAdapter.ViewHolder> {
    public static final String TAG = SwipeableRecyclerAdapter.class.getSimpleName();
    private int count = 20;
    private Context mContext;

    private EventListener mEventListener;
    private View.OnClickListener mItemViewOnClickListener;
    private View.OnClickListener mSwipeableViewContainerOnClickListener;


    public SwipeableRecyclerAdapter() {
        setHasStableIds(true);
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {
        mContext = viewGroup.getContext();
        LayoutInflater inflater = LayoutInflater.from(mContext);
        View view = inflater.inflate(R.layout.card_item, viewGroup, false);
        ViewHolder holder = new ViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {

        final int swipeState = holder.getSwipeStateFlags();

        if ((swipeState & RecyclerViewSwipeManager.STATE_FLAG_IS_UPDATED) != 0) {
            int bgResId;

            if ((swipeState & RecyclerViewSwipeManager.STATE_FLAG_IS_ACTIVE) != 0) {
                bgResId = R.color.bg_item_swiping_active_state;
            } else if ((swipeState & RecyclerViewSwipeManager.STATE_FLAG_SWIPING) != 0) {
                bgResId = R.color.bg_item_swiping_state;
            } else {
                bgResId = R.color.bg_item_normal_state;
            }

            Log.d(TAG, "bg = " + bgResId);
//            holder.mContainer.setBackgroundResource(bgResId);
        }
    }

    @Override
    public int getItemCount() {
        return 20;
    }

    public void setEventListener(EventListener listener) {
        mEventListener = listener;
    }

    @Override
    public int onGetSwipeReactionType(ViewHolder viewHolder, int x, int y) {
        return 0;
    }

    @Override
    public void onSetSwipeBackground(ViewHolder viewHolder, int type) {

    }

    @Override
    public int onSwipeItem(ViewHolder viewHolder, int result) {
        Log.d(TAG, "onSwipeItem(result = " + result + ")");

        switch (result) {
            // swipe right --- remove
            case RecyclerViewSwipeManager.RESULT_SWIPED_RIGHT:
                return RecyclerViewSwipeManager.AFTER_SWIPE_REACTION_REMOVE_ITEM;
            // swipe left -- pin
            case RecyclerViewSwipeManager.RESULT_SWIPED_LEFT:
                return RecyclerViewSwipeManager.AFTER_SWIPE_REACTION_MOVE_TO_SWIPED_DIRECTION;
            // other --- do nothing
            case RecyclerViewSwipeManager.RESULT_CANCELED:
            default:
                return RecyclerViewSwipeManager.AFTER_SWIPE_REACTION_DEFAULT;
        }
    }

    @Override
    public void onPerformAfterSwipeReaction(ViewHolder viewHolder, int result, int reaction) {
        Log.d(TAG, "onPerformAfterSwipeReaction(result = " + result + ", reaction = " + reaction + ")");

        final int position = viewHolder.getPosition();
    }


    public class ViewHolder extends AbstractSwipeableItemViewHolder {
        public ViewGroup mContainer;
        public ImageView mImageView;
        public TextView mTextView;

        public ViewHolder(View itemView) {
            super(itemView);
            mContainer = (ViewGroup) itemView;
            mImageView = (ImageView) itemView.findViewById(R.id.notification_icon);
            mTextView = (TextView) itemView.findViewById(R.id.notification_title);
        }

        @Override
        public View getSwipeableContainerView() {
            return mContainer;
        }
    }

    public interface EventListener {
        public void onItemRightSwipe(int position);

        public void onItemLeftSwipe(int position);

        public void onItemViewClicked(View view, boolean pinned);
    }
}

package test.materialdesign.adapters;

import android.annotation.TargetApi;
import android.app.Activity;
import android.app.ActivityOptions;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.support.v7.widget.RecyclerView;
import android.util.Pair;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import test.materialdesign.R;
import test.materialdesign.ui.activities.DetailActivity;

/**
 * Created by Alexandr on 12.02.2015.
 */
public class RecyclerCardAdapter extends RecyclerView.Adapter<RecyclerCardAdapter.ViewHolder> {
    public static final String TAG = RecyclerCardAdapter.class.getSimpleName();
    private Context mContext;
    private int count = 20;

    @Override
    public RecyclerCardAdapter.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        mContext = viewGroup.getContext();
        LayoutInflater inflater = LayoutInflater.from(mContext);
        View view = inflater.inflate(R.layout.card_item, viewGroup, false);
        ViewHolder holder = new ViewHolder(view);
        return holder;
    }

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {

    }

    @Override
    public int getItemCount() {
        return count;
    }

    public void addItem(int position) {
        count++;
        notifyItemInserted(position);
    }

    public void removeItem(int position) {
        count--;
        notifyItemRemoved(position);
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public ImageView mImageView;
        public TextView mTextView;

        public ViewHolder(View itemView) {
            super(itemView);
//            itemView.setOnClickListener(clickListener);
            mImageView = (ImageView) itemView.findViewById(R.id.notification_icon);
            mTextView = (TextView) itemView.findViewById(R.id.notification_title);
        }

        private View.OnClickListener clickListener = new View.OnClickListener() {
            @TargetApi(Build.VERSION_CODES.LOLLIPOP)
            @Override
            public void onClick(View v) {
                onTransition(mContext, new Pair<View, String>(mTextView, mTextView.getTransitionName()),
                        new Pair<View, String>(mImageView, mImageView.getTransitionName()));
            }
        };
    }

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    public void onTransition(Context context, Pair<View, String>... sharedElements) {
        Intent intent = new Intent(context, DetailActivity.class);
        ActivityOptions options =  ActivityOptions.makeSceneTransitionAnimation((Activity) context, sharedElements);
        mContext.startActivity(intent, options.toBundle());
    }
}

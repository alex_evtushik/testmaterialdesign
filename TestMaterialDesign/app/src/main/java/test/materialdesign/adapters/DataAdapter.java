package test.materialdesign.adapters;

import android.app.Activity;
import android.content.Context;
import android.database.Cursor;
import android.view.ActionMode;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.TextView;

import java.util.List;

import test.materialdesign.R;
import test.materialdesign.databases.employee.EmployeesSQLiteHelper.EmployeesDB;
import test.materialdesign.databases.employee.MainCursor;
import test.materialdesign.listeners.OnViewHolderListener;

/**
 * Created by Alexandr on 13.02.2015.
 */
public class DataAdapter extends SelectableAdapter<RecyclerView.ViewHolder> {
    public static final String TAG = DataAdapter.class.getSimpleName();
    public final int LIMIT = 20;
    private final int ITEM_VIEW_TYPE = 0;
    private final int FOOTER_VIEW_TYPE = 1;
    private Context mContext;
    private ActionMode mActionMode;
    private int mSelectCount = 0;

    private Cursor mCursor;
    private MainCursor mMainCursor;
    private OnViewHolderListener mViewHolderListener = null;
    private int requestCount = LIMIT;


    public DataAdapter(Cursor cursor) {
        mCursor = cursor;
    }

    public DataAdapter(MainCursor mainCursor) {
        mMainCursor = mainCursor;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        mContext = parent.getContext();
        RecyclerView.ViewHolder holder = null;
        if (viewType == ITEM_VIEW_TYPE) {
            LayoutInflater inflater = LayoutInflater.from(parent.getContext());
            View view = inflater.inflate(R.layout.employee_item, parent, false);
            holder = new ViewHolder(view);
        } else if (viewType == FOOTER_VIEW_TYPE) {
            LayoutInflater inflater = LayoutInflater.from(parent.getContext());
            View view = inflater.inflate(R.layout.progress_bar, parent, false);
            holder = new FooterViewHolder(view);
        }
        return holder;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, final int position) {
        if (holder instanceof ViewHolder) {
            mCursor.moveToPosition(position);
            ((ViewHolder) holder).mTextView.setText(mCursor.getString(mCursor.getColumnIndex(EmployeesDB.FULLNAME)));

            ((ViewHolder) holder).mSelectedOverlay.setVisibility(isSelectes(position) ? View.VISIBLE : View.INVISIBLE);

        } else if (holder instanceof FooterViewHolder) {
            ((FooterViewHolder) holder).mProgressBar.setIndeterminate(true);
        }

        if (position == (getItemCount() - 1) && mViewHolderListener != null) {
            if (requestCount <= getItemCount()) {
                requestCount = requestCount + LIMIT;
                mViewHolderListener.onRequestedLastItem(requestCount);
            }
        }
    }

    @Override
    public int getItemCount() {
        int count;
        if (mCursor == null)
            count = 0;
        else {
            if (requestCount == mCursor.getCount())
                count = requestCount + 1;
            else
                count = mCursor.getCount();
        }
        return count;
    }

    @Override
    public int getItemViewType(int position) {
        int type = ITEM_VIEW_TYPE;
        if (position == requestCount)
            type = FOOTER_VIEW_TYPE;
        return type;
    }

    public void changeCursor(Cursor cursor) {
        Cursor oldCursor = swapCursor(cursor);
        if (oldCursor != null) {
            oldCursor.close();
        }
    }

    public Cursor swapCursor(Cursor cursor) {
        if (mCursor == cursor) {
            return null;
        }
        Cursor oldCursor = mCursor;
        mCursor = cursor;
        if (cursor != null) {
            this.notifyDataSetChanged();
        }

        return oldCursor;
    }

    public void changeCursor(MainCursor cursor) {
        MainCursor oldCursor = swapCursor(cursor);
        if (oldCursor != null) {
            oldCursor.close();
        }
    }

    public MainCursor swapCursor(MainCursor cursor) {
        if (mMainCursor == cursor) {
            return null;
        }
        MainCursor oldCursor = mMainCursor;
        mMainCursor = cursor;
        if (cursor != null) {
            this.notifyDataSetChanged();
        }
        return oldCursor;
    }

    public void selection(int position) {
        toggleSelection(position);
        int count = getSelectedItemCount();

        if (count == 0)
            mActionMode.finish();
        else {
            mActionMode.setTitle(String.valueOf(count));
            mActionMode.invalidate();
        }
    }

    public void setOnViewHolderListener(OnViewHolderListener viewHolderListener) {
        mViewHolderListener = viewHolderListener;
    }

    private ActionMode.Callback callback = new ActionMode.Callback() {
        @Override
        public boolean onCreateActionMode(ActionMode actionMode, Menu menu) {
            actionMode.getMenuInflater().inflate(R.menu.selected_menu, menu);
            return true;
        }

        @Override
        public boolean onPrepareActionMode(ActionMode actionMode, Menu menu) {
            return false;
        }

        @Override
        public boolean onActionItemClicked(ActionMode actionMode, MenuItem menuItem) {
            switch (menuItem.getItemId()) {
                case R.id.select:

                    List<Integer> selectedPos = getSelectedItems();
                    Log.d(TAG, "SELECTED count = " + selectedPos.size());
                    for (int i = selectedPos.size() - 1; i >= 0; i-- ) {
                        Log.d(TAG, "SELECTED item = " + selectedPos.get(i));
                    }

                    actionMode.finish();
                    return true;
                default:
                    return false;
            }
        }

        @Override
        public void onDestroyActionMode(ActionMode actionMode) {
            mActionMode = null;
            clearSelection();
        }
    };

    public class ViewHolder extends RecyclerView.ViewHolder {
        public TextView mTextView;
        public View mSelectedOverlay;

        public ViewHolder(View itemView) {
            super(itemView);

            mTextView = (TextView) itemView.findViewById(R.id.fullname);
            mSelectedOverlay = itemView.findViewById(R.id.selected_overlay);

            itemView.setOnClickListener(clickListener);
            itemView.setOnLongClickListener(longClickListener);
        }

        private View.OnClickListener clickListener = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.d(TAG, "ON CLICK ITEM " + getPosition());

                if (mActionMode != null)
                    selection(getPosition());
            }
        };

        private View.OnLongClickListener longClickListener = new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                Log.d(TAG, "ON LONG CLICK ITEM " + getPosition());
                if (mActionMode == null) {
                    mActionMode = ((Activity) mContext).startActionMode(callback);
                    selection(getPosition());
                }

                return true;
            }
        };
    }

    public class FooterViewHolder extends RecyclerView.ViewHolder {
        public ProgressBar mProgressBar;

        public FooterViewHolder(View itemView) {
            super(itemView);
            mProgressBar = (ProgressBar) itemView.findViewById(R.id.progress_bar);
        }
    }
}
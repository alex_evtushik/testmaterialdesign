package test.materialdesign.adapters;

import android.annotation.TargetApi;
import android.app.ActivityOptions;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.support.v7.widget.RecyclerView;
import android.util.Pair;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import test.materialdesign.R;
import test.materialdesign.ui.activities.PhotoActivity;
import test.materialdesign.adapters.RecyclerAdapter.ViewHolder;

/**
 * Created by Alexandr on 12.02.2015.
 */
public class RecyclerAdapter extends RecyclerView.Adapter<ViewHolder> {
    private Context mContext;
    private int[] images = { R.drawable.img, R.drawable.img, R.drawable.img_2, R.drawable.img_2, R.drawable.img, R.drawable.img_2, R.drawable.img, R.drawable.img, R.drawable.img, R.drawable.img,
            R.drawable.img_2, R.drawable.img_2, R.drawable.img, R.drawable.img_2, R.drawable.img_2 };
    private int count = 20;

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        mContext = viewGroup.getContext();
        LayoutInflater inflater = LayoutInflater.from(mContext);
        View view = inflater.inflate(R.layout.image_item, viewGroup, false);
        ViewHolder holder = new ViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(final ViewHolder viewHolder, final int i) {
        viewHolder.image.setImageResource(images[i]);
        viewHolder.image.setOnClickListener(new View.OnClickListener() {

            @TargetApi(Build.VERSION_CODES.LOLLIPOP)
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(mContext, PhotoActivity.class);
                intent.putExtra("img_res", images[i]);
                ActivityOptions options =  ActivityOptions.makeSceneTransitionAnimation((android.app.Activity) mContext,
                        new Pair<View, String>(viewHolder.image, viewHolder.image.getTransitionName()));

                mContext.startActivity(intent, options.toBundle());
            }
        });
    }

    @Override
    public int getItemCount() {
        return images.length;
    }

    public void addItem(int position) {
        //notifyItemInserted(position);
    }

    public void removeItem(int position) {
        notifyItemRemoved(position);
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public ImageView image;

        public ViewHolder(View itemView) {
            super(itemView);
            image = (ImageView) itemView.findViewById(R.id.image_view);
        }
    }
}

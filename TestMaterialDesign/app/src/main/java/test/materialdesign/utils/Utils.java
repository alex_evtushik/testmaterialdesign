package test.materialdesign.utils;

import android.content.ContentValues;
import android.content.Context;

import test.materialdesign.databases.employee.EmployeesSQLiteHelper;

/**
 * Created by Alexandr on 18.02.2015.
 */
public class Utils {

    private Utils() {}

    public static void fillDB(Context context, int count) {
        for (int i = 0; i < count; i++) {
            ContentValues values = new ContentValues();
            values.put(EmployeesSQLiteHelper.EmployeesDB.FULLNAME, "Name " + (i + 1));
            context.getContentResolver().insert(EmployeesSQLiteHelper.EmployeesDB.CONTENT_URI, values);
        }
    }
}

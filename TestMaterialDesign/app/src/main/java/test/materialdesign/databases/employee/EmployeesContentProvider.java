package test.materialdesign.databases.employee;

import android.content.ContentProvider;
import android.content.ContentValues;
import android.content.UriMatcher;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteQueryBuilder;
import android.net.Uri;
import android.text.TextUtils;

import java.util.Arrays;
import java.util.HashSet;

import test.materialdesign.databases.employee.EmployeesSQLiteHelper.EmployeesDB;

/**
 * Created by Alexandr on 13.02.2015.
 */
public class EmployeesContentProvider extends ContentProvider {
    private EmployeesSQLiteHelper mHelper;
    private static final int EMPLOYEES = 1;
    private static final int EMPLOYEES_ID = 2;

    private static final UriMatcher sURIMatcher = new UriMatcher(UriMatcher.NO_MATCH);
    static {
        sURIMatcher.addURI(EmployeesDB.AUTHORITY, EmployeesDB.BASE_PATH, EMPLOYEES);
        sURIMatcher.addURI(EmployeesDB.AUTHORITY, EmployeesDB.BASE_PATH + "/#", EMPLOYEES_ID);
    }

    @Override
    public boolean onCreate() {
        mHelper = new EmployeesSQLiteHelper(getContext());
        return false;
    }

    @Override
    public Cursor query(Uri uri, String[] projection, String selection, String[] selectionArgs, String sortOrder) {
        SQLiteQueryBuilder queryBuilder = new SQLiteQueryBuilder();
        checkColumns(projection);
        queryBuilder.setTables(EmployeesDB.TABLE);
        switch (sURIMatcher.match(uri)) {
            case EMPLOYEES:
                break;
            case EMPLOYEES_ID:
                queryBuilder.appendWhere(EmployeesDB.ID + "=" + uri.getLastPathSegment());
                break;
            default:
                throw new IllegalArgumentException("Unknown URI: " + uri);
        }

        SQLiteDatabase db = mHelper.getWritableDatabase();
        Cursor cursor = queryBuilder.query(db, projection, selection, selectionArgs, null, null, sortOrder);
        cursor.setNotificationUri(getContext().getContentResolver(), uri);

        return cursor;
    }

    @Override
    public String getType(Uri uri) {
        switch (sURIMatcher.match(uri)) {
            case EMPLOYEES:
                return EmployeesDB.CONTENT_TYPE;
            case EMPLOYEES_ID:
                return EmployeesDB.CONTENT_ITEM_TYPE;
            default:
                throw new IllegalArgumentException("Unknown URI: " + uri);
        }
    }

    @Override
    public Uri insert(Uri uri, ContentValues values) {
        SQLiteDatabase db = mHelper.getWritableDatabase();
        long id = 0;
        switch (sURIMatcher.match(uri)) {
            case EMPLOYEES:
                id = db.insert(EmployeesDB.TABLE, null, values);
                break;
            case EMPLOYEES_ID:
                break;
            default:
                throw new IllegalArgumentException("Unknown URI: " + uri);
        }
        getContext().getContentResolver().notifyChange(uri, null);
        return Uri.parse(EmployeesDB.BASE_PATH + "/" + id);
    }

    @Override
    public int delete(Uri uri, String selection, String[] selectionArgs) {
        SQLiteDatabase db = mHelper.getWritableDatabase();
        int rowsDeleted = 0;
        switch (sURIMatcher.match(uri)) {
            case EMPLOYEES:
                rowsDeleted = db.delete(EmployeesDB.TABLE, selection, selectionArgs);
                break;
            case EMPLOYEES_ID:
                String id = uri.getLastPathSegment();
                if (TextUtils.isEmpty(selection))
                    rowsDeleted = db.delete(EmployeesDB.TABLE, EmployeesDB.ID + "=" + id, null);
                else
                    rowsDeleted = db.delete(EmployeesDB.TABLE, EmployeesDB.ID + "=" + id + " and " + selection, selectionArgs);
                break;
            default:
                throw new IllegalArgumentException("Unknown URI: " + uri);
        }
        getContext().getContentResolver().notifyChange(uri, null);
        return rowsDeleted;
    }

    @Override
    public int update(Uri uri, ContentValues values, String selection, String[] selectionArgs) {
        SQLiteDatabase db = mHelper.getWritableDatabase();
        int rowsUpdated = 0;
        switch (sURIMatcher.match(uri)) {
            case EMPLOYEES:
                rowsUpdated = db.update(EmployeesDB.TABLE, values, selection, selectionArgs);
                break;
            case EMPLOYEES_ID:
                String id = uri.getLastPathSegment();
                if (TextUtils.isEmpty(selection))
                    rowsUpdated = db.update(EmployeesDB.TABLE, values, EmployeesDB.ID + "=" + id, null);
                else
                    rowsUpdated = db.update(EmployeesDB.TABLE, values, EmployeesDB.ID + "=" + id + " and " + selection, selectionArgs);
                break;
            default:
                throw new IllegalArgumentException("Unknown URI: " + uri);
        }
        getContext().getContentResolver().notifyChange(uri, null);
        return rowsUpdated;
    }

    private void checkColumns(String[] projection) {
        if (projection != null) {
            HashSet<String> requestedColumns = new HashSet<>(Arrays.asList(projection));
            HashSet<String> availableColumns = new HashSet<>(Arrays.asList(EmployeesDB.DEFAULT_PROJECTION));

            // check if all columns which are requested are available
            if (!availableColumns.containsAll(requestedColumns))
                throw new IllegalArgumentException("Unknown columns in projection");
        }
    }
}

package test.materialdesign.databases.employee;

import android.database.AbstractCursor;
import android.database.Cursor;
import android.util.Log;

/**
 * Created by Alexandr on 22.02.2015.
 */
public class MainCursor extends AbstractCursor {
    public static final String TAG = MainCursor.class.getSimpleName();
    private Cursor mCursor;

    public MainCursor(Cursor cursor) {
        setCursor(cursor);
    }

    public void setCursor(Cursor cursor) {
        Log.d(TAG, "setCursor = " + cursor.toString());
        mCursor = cursor;
    }

    @Override
    public int getCount() {
        Log.d(TAG, "getCount = " + mCursor.getCount());
        return mCursor.getCount();
    }

    @Override
    public String[] getColumnNames() {
        return mCursor.getColumnNames();
    }

    @Override
    public String getString(int column) {
        Log.d(TAG, "getString = " + mCursor.getString(column));
        return mCursor.getString(column);
    }

    @Override
    public short getShort(int column) {
        return mCursor.getShort(column);
    }

    @Override
    public int getInt(int column) {
        Log.d(TAG, "getInt = " + mCursor.getInt(column));
        return mCursor.getInt(column);
    }

    @Override
    public long getLong(int column) {
        return mCursor.getLong(column);
    }

    @Override
    public float getFloat(int column) {
        return mCursor.getFloat(column);
    }

    @Override
    public double getDouble(int column) {
        return mCursor.getDouble(column);
    }

    @Override
    public boolean isNull(int column) {
        return mCursor.isNull(column);
    }

    @Override
    public int getColumnIndex(String columnName) {
        return mCursor.getColumnIndex(columnName);
    }

    public void mainMoveToPosition(int position) {
        mCursor.moveToPosition(position);
    }
}

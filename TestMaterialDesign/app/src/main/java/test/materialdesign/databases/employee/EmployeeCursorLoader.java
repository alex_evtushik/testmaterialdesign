package test.materialdesign.databases.employee;

import android.content.Context;
import android.content.CursorLoader;
import android.content.Loader;
import android.database.Cursor;
import android.os.Bundle;
import android.app.LoaderManager.LoaderCallbacks;
import test.materialdesign.databases.employee.EmployeesSQLiteHelper.EmployeesDB;

import test.materialdesign.adapters.DataAdapter;

/**
 * Created by Alexandr on 13.02.2015.
 */
public class EmployeeCursorLoader implements LoaderCallbacks<Cursor> {
    private Context mContext;
    private DataAdapter mDataAdapter;
    private int mLimit;
    private int mOffset;

    public EmployeeCursorLoader(Context context, DataAdapter dataAdapter, int limit) {
        mContext = context;
        mDataAdapter = dataAdapter;
        mLimit = limit;
    }

    public EmployeeCursorLoader(Context context, DataAdapter dataAdapter, int limit, int offset) {
        mContext = context;
        mDataAdapter = dataAdapter;
        mLimit = limit;
        mOffset = offset;
    }

    @Override
    public Loader<Cursor> onCreateLoader(int id, Bundle args) {
        //String sortOrder = String.format("%s asc limit %d offset %d", EmployeesDB.ID, mLimit, mOffset);

        String sortOrder = String.format("%s asc limit %d", EmployeesDB.ID, mLimit);

        CursorLoader cursorLoader = new CursorLoader(mContext, EmployeesDB.CONTENT_URI,
                EmployeesDB.DEFAULT_PROJECTION, null, null, sortOrder);

        return cursorLoader;
    }

    @Override
    public void onLoadFinished(Loader<Cursor> loader, Cursor data) {
        mDataAdapter.swapCursor(data);
    }

    @Override
    public void onLoaderReset(Loader<Cursor> loader) {
        mDataAdapter.swapCursor(null);
    }
}

package test.materialdesign.databases.employee;

import android.content.ContentResolver;
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.net.Uri;

import java.util.Locale;

/**
 * Created by Alexandr on 13.02.2015.
 */
public class EmployeesSQLiteHelper extends SQLiteOpenHelper {
    private static final int DB_VERSION = 1;
    private static final String DB_NAME = "employees.db";

    public EmployeesSQLiteHelper(Context context) {
        super(context, DB_NAME, null, DB_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(EmployeesDB.CREATE_TABLE_QUERY);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS " + EmployeesDB.TABLE);
        this.onCreate(db);
    }

    public static class EmployeesDB {
        public static final String TABLE = "employees";
        public static final String ID = "_id";
        public static final String FULLNAME = "fullname";

        public static final String AUTHORITY = "test.materialdesign.content_provider";
        public static final String BASE_PATH = "employees";

        public static final Uri CONTENT_URI = Uri.parse("content://" + AUTHORITY + "/" + BASE_PATH);
        public static final String CONTENT_TYPE = ContentResolver.CURSOR_DIR_BASE_TYPE + "/employees";
        public static final String CONTENT_ITEM_TYPE = ContentResolver.CURSOR_ITEM_BASE_TYPE + "/employee";

        public static final String[] DEFAULT_PROJECTION = { EmployeesDB.ID, EmployeesDB.FULLNAME };

        private static final String CREATE_TABLE_PATTERN =
                "CREATE TABLE %s ( " +
                        " %s INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT," +
                        " %s STRING NOT NULL)" ;

        private static final String CREATE_TABLE_QUERY = String.format(Locale.ENGLISH, CREATE_TABLE_PATTERN,
                TABLE,
                ID,
                FULLNAME);
    }
}

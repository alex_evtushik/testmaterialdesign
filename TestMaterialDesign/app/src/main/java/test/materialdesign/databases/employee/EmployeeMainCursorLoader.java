package test.materialdesign.databases.employee;

import android.app.LoaderManager;
import android.content.Context;
import android.content.CursorLoader;
import android.content.Loader;
import android.database.Cursor;
import android.os.Bundle;

import test.materialdesign.adapters.DataAdapter;

/**
 * Created by Alexandr on 22.02.2015.
 */
public class EmployeeMainCursorLoader implements LoaderManager.LoaderCallbacks<Cursor> {
    private Context mContext;
    private DataAdapter mDataAdapter;
    private int mLimit;
    private int mOffset;

    public EmployeeMainCursorLoader(Context context, DataAdapter dataAdapter, int limit, int offset) {
        mContext = context;
        mDataAdapter = dataAdapter;
        mLimit = limit;
        mOffset = offset;
    }

    @Override
    public Loader<Cursor> onCreateLoader(int id, Bundle args) {
        String sortOrder = String.format("%s asc limit %d offset %d", EmployeesSQLiteHelper.EmployeesDB.ID, mLimit, mOffset);

        //String sortOrder = String.format("%s asc limit %d", EmployeesDB.ID, mLimit);

        CursorLoader cursorLoader = new CursorLoader(mContext, EmployeesSQLiteHelper.EmployeesDB.CONTENT_URI,
                EmployeesSQLiteHelper.EmployeesDB.DEFAULT_PROJECTION, null, null, null);

        return cursorLoader;
    }

    @Override
    public void onLoadFinished(Loader<Cursor> loader, Cursor data) {
        MainCursor mainCursor = new MainCursor(data);
        mDataAdapter.swapCursor(mainCursor);
    }

    @Override
    public void onLoaderReset(Loader<Cursor> loader) {
        mDataAdapter.swapCursor(null);
    }
}
